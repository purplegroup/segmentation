""" The  script allows to combine the different backgrounds with different lego blocks. 

Make sure the backgrounds are in the folder table_labeled and the legos are contained in the legos/

The new generated data will be saved in the file labeled"""

import matplotlib.pyplot as plt
import numpy as np
import cv2
import os
import pdb



red = np.array([[[0,0,255]]])
blue = np.array([[[255,0,0]]])


table_file = "table_labeled/"
lego_file = "legos/"
labeled_file = "labeled"

tables = os.listdir(table_file)
tables = set([x.split("_")[0] for x in tables if "table" in x])

legos = os.listdir(lego_file)
legos = set([x.split("_")[0] for x in legos if "lego" in x])


def onclick(event):
    global x_cord
    global y_cord 
    
    x_cord = event.x
    y_cord = event.y 
    #print('button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
     #     (event.button, event.x, event.y, event.xdata, event.ydata))


image_num = 600		
img_shape = (480,640)
for table in tables:
    table_img = cv2.imread(os.path.join(table_file, table+"_original.png"))
    table_mask = cv2.imread(os.path.join(table_file, table+"_table.png"))
    labeled = np.zeros(img_shape)
    labeled[np.where(np.all(table_mask ==red, axis=-1))] = 2

    fig = plt.figure(frameon = False)
    fig.set_size_inches(8.3,8.3*1.33)
    ax = fig.add_subplot(111)    
    ax.imshow(table_img[...,::-1])
    cid = fig.canvas.mpl_connect('button_press_event', onclick)
    plt.show()
    x_cord = x_cord - 104
    y_cord = y_cord - 77
    print(x_cord, y_cord)

    top = int(480 - y_cord)
    left = int(x_cord)

    for lego in list(legos):
    	print(lego)
    	bench = cv2.imread(os.path.join(lego_file, lego+"_original.png"))
    	lego_mask = cv2.imread(os.path.join(lego_file, lego+"_lego.png"))

    	image_mask = labeled.copy()
    	lego_mask_only = np.zeros(img_shape)
    	lego_mask_only[np.where(np.all(lego_mask ==blue, axis=-1))] = 1

    	indices = np.where(lego_mask_only == 1)

    	top_cord = np.clip(top + indices[0]-np.min(indices[0]), 0 , 479)
    	left_cord = np.clip(left +indices[1]-np.min(indices[1]),0 , 639)
    	image = table_img.copy()

    	image[(top_cord, left_cord)] = bench[(indices[0], indices[1])]
    	image_mask[(top_cord, left_cord)] = 1

    	#plt.imshow(image[...,::-1])
    	#plt.show()
    	#plt.imshow(image_mask)
    	#plt.show()

    	cv2.imwrite(os.path.join(labeled_file, str(image_num)+"_original.png"), image)
    	plt.imshow(image_mask)
    	plt.savefig(os.path.join(labeled_file, str(image_num)+"_pixellabel.png"))
    	np.save(os.path.join(labeled_file, str(image_num)+"_pixellabel.npy"), image_mask.astype("uint8"))
    	image_num = image_num+1

    	#pdb.set_trace()	
    	#labeled[np.where(np.all(table_mask ==red, axis=-1))] = 2
