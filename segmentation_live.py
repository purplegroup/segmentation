import numpy as np
import tensorflow as tf
import math
import time
import cv2
import pdb
config = tf.ConfigProto()
config.gpu_options.allow_growth = True



learn_rate = 5e-3
batch_size = 7
learn_decay = 1 - 1e-5
num_epochs = 250
np.set_printoptions(threshold=np.nan)
tf.reset_default_graph()
g_1 = tf.Graph()
sess1=tf.Session(graph=g_1, config=config)

with g_1.as_default():
    X = tf.placeholder(tf.float32, [None,240,240,3])
    def model(X):
        Wconv1 = tf.get_variable('Wconv1',shape=[3,3,3,128],initializer=tf.contrib.layers.xavier_initializer())
        B1=tf.get_variable('Bconv1',shape=[128])
        Wconv2 = tf.get_variable('Wconv2',shape=[3,3,128,64],initializer=tf.contrib.layers.xavier_initializer())
        B2 = tf.get_variable('Bconv2',shape=[64])
        Wconv3 = tf.get_variable('Wconv3',shape=[3,3,64,3],initializer=tf.contrib.layers.xavier_initializer())
        B3 = tf.get_variable('Bconv3',shape=[3])
        
        a1=tf.nn.atrous_conv2d(X,Wconv1,2,padding='SAME')+B1
        h1=tf.nn.relu(a1)
        
        a2=tf.nn.atrous_conv2d(h1,Wconv2,2,padding='SAME')+B2 #strides=[1,1,1,1]
        h2=tf.nn.relu(a2)
        
        a3=tf.nn.atrous_conv2d(h2,Wconv3,2,padding='SAME')+B3 #strides=[1,1,1,1]
        h3=tf.nn.relu(a3)
        yout=tf.reshape(h3,[-1,3])###########yout.shape=[bach*480*480,3]
        return yout
    y_out=model(X)    
    saver1 = tf.train.Saver()
    saver1.restore(sess1, "checkpoint/checkpoints.ckpt")
    

def run_model1(session,predict,Xd):
        
    variables=[predict]
    ################Session###################################
    feed_dict={X:Xd}
    y_output=session.run(variables,feed_dict=feed_dict)
    ################Session###################################
    
    ####################CFD###################################              
    y_pridict=np.argmax(y_output,axis=-1)
    y_pridict=y_pridict.reshape((240,240))
    temp2=y_pridict.astype('int8')
    tempcv2 = (temp2/2.0*255).astype('uint8')
    cv2.imshow('without classifier',tempcv2)
    cv2.waitKey(33)
    return temp2




from threading import Thread, Lock
import time
camera=cv2.VideoCapture(2)
lock = Lock()

def grab_frames():
    while not lock.locked():
        camera.grab()
        time.sleep(0.01)
    print("Finished grabbing")
        
t = Thread(target=grab_frames)
t.start()

time.sleep(2)


with tf.device("/gpu:0") as dev:
    print('Testing')
    for i in range(1500):
        return_value, image = camera.retrieve()
        print(return_value)
        image =cv2.resize(image,(240,240))
        cv2.imshow("original",image)
        img=image
        image =(image/255.-0.5)*2
        image= np.expand_dims(image, axis=0)
        plabel=run_model1(sess1,y_out,image)
        
        print(i)
cv2.destroyAllWindows()

