import numpy as np
import tensorflow as tf
import math
import time
import cv2
import pdb
from skimage import measure
import matplotlib.pyplot as plt
config = tf.ConfigProto()
config.gpu_options.allow_growth = True



learn_rate = 5e-3
batch_size = 7
learn_decay = 1 - 1e-5
num_epochs = 250
np.set_printoptions(threshold=np.nan)
tf.reset_default_graph()
g_1 = tf.Graph()
g_2 = tf.Graph()
sess1=tf.Session(graph=g_1, config=config)
sess2=tf.Session(graph=g_2, config=config)

with g_1.as_default():
    X = tf.placeholder(tf.float32, [None,240,240,3])
    def model(X):
        Wconv1 = tf.get_variable('Wconv1',shape=[3,3,3,128],initializer=tf.contrib.layers.xavier_initializer())
        B1=tf.get_variable('Bconv1',shape=[128])
        Wconv2 = tf.get_variable('Wconv2',shape=[3,3,128,64],initializer=tf.contrib.layers.xavier_initializer())
        B2 = tf.get_variable('Bconv2',shape=[64])
        Wconv3 = tf.get_variable('Wconv3',shape=[3,3,64,3],initializer=tf.contrib.layers.xavier_initializer())
        B3 = tf.get_variable('Bconv3',shape=[3])
        
        a1=tf.nn.atrous_conv2d(X,Wconv1,2,padding='SAME')+B1
        h1=tf.nn.relu(a1)
        
        a2=tf.nn.atrous_conv2d(h1,Wconv2,2,padding='SAME')+B2 #strides=[1,1,1,1]
        h2=tf.nn.relu(a2)
        
        a3=tf.nn.atrous_conv2d(h2,Wconv3,2,padding='SAME')+B3 #strides=[1,1,1,1]
        h3=tf.nn.relu(a3)
        yout=tf.reshape(h3,[-1,3])###########yout.shape=[bach*480*480,3]
        return yout
    y_out=model(X)    
    saver1 = tf.train.Saver()
    saver1.restore(sess1, "checkpoint/checkpoints.ckpt")
    
with g_2.as_default():
    X2 = tf.placeholder(tf.float32, [None,50,50,3])
    def model(X2):
        initializer = tf.contrib.layers.xavier_initializer()
        first_conv = tf.layers.conv2d(X2, 8, 3, strides=1, padding='valid',
                                     activation=tf.nn.relu, 
                                     kernel_initializer=initializer,
                                     name='First_convolution')
        second_conv = tf.layers.conv2d(first_conv, 4, 3, strides=1, padding='valid',
                                      activation=tf.nn.relu, 
                                      kernel_initializer=initializer,
                                      name='Second_convolution')
        max_pool = tf.layers.max_pooling2d(second_conv, 2, 2, padding='valid', name='Max_pool')
        flatten = tf.reshape(max_pool, [-1, 2116])
        #fc_layer = tf.layers.dense(flatten, 256, activation=tf.nn.relu,
                                  #kernel_initializer=initializer,
                                  #name='Dense_layer')
        fc=tf.layers.dropout(flatten, rate=0.85, training=False)        
        output = tf.layers.dense(fc, 2, activation=None,
                                kernel_initializer=initializer,
                                name='Predictions')
        return output
    y_out2=model(X2)    
    saver2 = tf.train.Saver()
    saver2.restore(sess2, "checkpoint_class/checkpoints.ckpt")

def run_model1(session,predict,Xd):
        
    variables=[predict]
    ################Session###################################
    feed_dict={X:Xd}
    y_output=session.run(variables,feed_dict=feed_dict)
    ################Session###################################
    
    ####################CFD###################################              
    y_pridict=np.argmax(y_output,axis=-1)
    y_pridict=y_pridict.reshape((240,240))
    temp2=y_pridict.astype('int8')
    tempcv2 = (temp2/2.0*255).astype('uint8')
    cv2.imshow('without classifier',tempcv2)
    cv2.waitKey(33)
    return temp2

def run_model2(session,predict,Xd):
    classification=tf.argmax(predict,1)    
    variables=[classification]
    ################Session###################################
    feed_dict={X2:Xd}
    y_output=session.run(variables,feed_dict=feed_dict)
    return y_output

def get_lego_crops(img, plabel, frame):
    new_plabel = np.zeros_like(plabel)
    new_plabel[np.where(plabel ==1)] = 1
    labels = measure.label(new_plabel, neighbors=8, background=0)
    lego_crops = dict()
    template_size = 50
    #pdb.set_trace()
    
    
    for label in np.unique(labels):
        if label == 0:
            continue
            
        labelMask = np.zeros(new_plabel.shape, dtype="uint8")
        labelMask[labels == label] = 1
        numPixels = cv2.countNonZero(labelMask)
        indices = np.where(labelMask == 1)
        #pdb.set_trace()
        
        if numPixels >50 and numPixels<50000:
            #pdb.set_trace()
            block_only = np.zeros_like(img)
            block_only[indices] = img[indices]
            
            contours = cv2.findContours(labelMask.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
            
            
            contour_list = list()
            
            
            for conts in contours[1]:
                contour_list.append(np.squeeze(conts))
            
            
            contours = np.vstack(contour_list)
            left_min , top_min  = contours.min(axis = 0)
            left_max , top_max  = contours.max(axis = 0)
            if (top_max -top_min) <1 or (left_max - left_min)<1 or ((top_max -top_min) * (left_max - left_min) <0):
                continue
            
            cropped = block_only[top_min:top_max, left_min:left_max]#[...,::-1]
            resize_shape = tuple((np.array(cropped.shape)/max(cropped.shape)*template_size).astype("uint8"))
            resized_crop = cv2.resize(cropped.copy(), (resize_shape[1], resize_shape[0])) 
            template = np.zeros((template_size,template_size,3))
            
            #plt.imshow(resized_crop)
            #plt.show()
            
            template[int((template_size - resize_shape[0])/2):int((template_size - resize_shape[0])/2)+resize_shape[0], int((template_size - resize_shape[1])/2):resize_shape[1]+int((template_size - resize_shape[1])/2),:] = resized_crop.copy()
            
            lego_crops[label] = template.astype("uint8")
            #cv2.imwrite("testing/"+ str(frame+938)+"_"+str(label)+".png",template.astype("uint8"))
            
    return lego_crops,labels
            
            

            #plt.imshow(lego_crops[label])
            #plt.show()
  

#camera=cv2.VideoCapture(0)
from threading import Thread, Lock
import time
camera=cv2.VideoCapture(2)
lock = Lock()

def grab_frames():
    while not lock.locked():
        camera.grab()
        time.sleep(0.01)
    print("Finished grabbing")
        
t = Thread(target=grab_frames)
t.start()

time.sleep(2)


with tf.device("/gpu:0") as dev:
    print('Testing')
    for i in range(1500):
        return_value, image = camera.retrieve()#camera.read()
        print(return_value)
        image =cv2.resize(image,(240,240))
        cv2.imshow("original",image)
        img=image
        image =(image/255.-0.5)*2
        image= np.expand_dims(image, axis=0)
        plabel=run_model1(sess1,y_out,image)
        #pdb.set_trace()q
        lego_crops,labels=get_lego_crops(img, plabel, i)
        #pdb.set_trace()
        for crop_key in lego_crops.keys():
            img = lego_crops[crop_key]
            img = (img / 255.0 -0.5)*2
            #pdb.set_trace()
            a=run_model2(sess2,y_out2,np.expand_dims(img, axis =0))
            print(a)
            if a[0] == 0:
                temp_mask = np.ones_like(plabel)
                temp_mask[np.where(labels == crop_key)] = 0
                plabel = plabel * temp_mask
            
        cv2.imshow('With Classifier', (plabel*255/2).astype('uint8'))
        
        print(i)
#camera.release()
cv2.destroyAllWindows()

