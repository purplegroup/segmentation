This is part of the VW Deep Learning and Robotics Challenge conducted between 11-09-2017 to 13-10-2017 at the VW Data Lab, Munich.

The repository contains all the jupyter-notebook scripts of the Segmentation and the Classification. 
All the pretrained weights are stored in the "checkpoint*" files. The file with "live" is used for the testing of the segmentation and classification in live.
The file with "train" is used to train your own weights with your own data.

The theory behind the software can be found in the purple teams blog:
http://dlrc.pages.argmax.ai/blog/purple-team/

The data used for training is available here:
https://mega.nz/#!cWwDTZoD!rds1oVAuxHpYwLPwk5t7WCgBQUB6vp9gn5PugyLzDr4
